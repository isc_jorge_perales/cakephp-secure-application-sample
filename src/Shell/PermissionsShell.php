<?php
namespace App\Shell;

use Cake\Console\Shell;
use Cake\Utility\Xml;

/**
 * Permissions shell command.
 */
class PermissionsShell extends Shell
{
    public $interactive = false;


    /**
     * Initialize method.
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();        
        $this->loadModel('Modules');
    }
    
    /**
     * main() method.
     *
     * @return bool|int|null Success or error code.
     */
    public function main()
    {
        $this->out('Importing permissions.');
        $this->savePermissions();
    }
    
    /**
     * Save permissions to database.
     */
    private function savePermissions()
    {
        $permissions = $this->getXmlAsArray();
        
        foreach ($permissions['Security']['Module'] as $molduleNode) {
            $this->saveModule($molduleNode['@name'], function ($module) use ($molduleNode) {
                $this->saveActions($module, $molduleNode['Action']);                
            });
        }
        
        $this->writePathsFile($permissions['Security']['Module']);
    }
    
    /**
     * Save modules
     *
     * @param string $moduleName Module name.
     * @param callable $callback Callback function.
     */
    private function saveModule($moduleName, callable $callback)
    {
        $module = $this->Modules->findByName($moduleName)->first();
        
        if (is_null($module)) {
            $module = $this->Modules->newEntity();
            $module->name = $moduleName;
            
            if ($this->Modules->save($module)) {
                $callback($module);
            }
        } else {
            $callback($module);
        }
    }
    
    /**
     * Save permissions (if not exist).
     *
     * @param \App\Model\Entity\Module $module Module to which belong the permissions.
     * @param array $actions Array of actions/permissions.
     */
    private function saveActions($module, $actions)
    {
        $actions = isset($actions[0]) ? $actions : [$actions];
        
        foreach ($actions as $action) {
            $permission = $this->Modules->Permissions->find()
                ->where([
                    'module_id' => $module->id,
                    'name' => $action['@name']
                ])
                ->first();
            
            if (is_null($permission)) {
                $this->saveSiglePermission($action, $module->id);
            }
        }
    }
    
    /**
     * Save individual permission.
     *
     * @param array $action Action/Permission
     * @param int $moduleId Module to which the permission belongs
     */
    private function saveSiglePermission($action, $moduleId)
    {
        $permission = $this->Modules->Permissions->newEntity();
        
        $permission->name       = $action['@name'];
        $permission->action     = $action['@action'];
        $permission->controller = $action['@controller'];
        $permission->module_id  = $moduleId;
        
        $this->Modules->Permissions->save($permission);
    }

    /*
     * Get location of the XML file
     * 
     * @return string Path.
     */
    private function getXmlPath()
    {       
        return ROOT . DS . 'config' . DS . 'permissions.xml';
    }
    
    /*
     * Get XML document from file
     * 
     * @return \SimpleXMLElement|\DOMDocument SimpleXMLElement or DOMDocument
     */
    private function getXmlFile()
    {        
        return Xml::build($this->getXmlPath());
    }    
    
    /**
     * Get XML structure as an array.
     *
     * @return array Array representation of the XML structure.
     */
    private function getXmlAsArray()
    {
        return Xml::toArray($this->getXmlFile());
    }
    
    
    /**
     * Write paths file
     *
     * @param array $modules Array of modules.
     */
    private function writePathsFile($modules)
    {        
        $contents = "<?php\n"
                  . "return [\n"
                  . "    'protected_paths' => [\n";
        
        foreach ($modules as $molduleNode) {
            $actions = isset($molduleNode['Action'][0]) ? 
                $molduleNode['Action'] : [$molduleNode['Action']];
            
            foreach ($actions as $action) {
                $path = strtolower($action['@controller'] . '.' . $action['@action']);
                $contents .= "        '{$path}',\n";
            }
        }
        
        $contents .= "    ]\n"
                  . "];\n";
        
        $this->createFile(ROOT . DS . 'config' . DS . 'secure_paths.php', $contents);
    }
}
