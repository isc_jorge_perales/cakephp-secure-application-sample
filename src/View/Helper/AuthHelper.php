<?php
namespace App\View\Helper;

use Cake\View\Helper;

class AuthHelper extends Helper {

    /**
     * Verify if the user can access to the requested action
     *
     * @param string|array $resource Requested resource.
     * @return boolean True is can. False otherwise.
     */
    public function check($resource)
    {
        $permissions = $this->request->session()->read('Auth.User.permissions');
        $isRoot      = (boolean) $this->request->session()->read('Auth.User.is_root');

        if ($isRoot) {
            return true;
        }

        if (!is_array($resource)) {
            return in_array($resource, $permissions);
        }

        if (is_array($resource)) {
            return count(array_intersect($resource, $permissions));
        }

        return false;
    }
}
