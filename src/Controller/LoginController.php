<?php
namespace App\Controller;

use App\Controller\AppController;
use App\Utility\Secure;
use Cake\I18n\Time;
use Cake\Core\Configure;

/**
 * Login Controller
 *
 * @property \App\Model\Table\LoginTable $Login
 */
class LoginController extends AppController
{
    
    /**
     * Initialize method
     */
    public function initialize() {
        parent::initialize();
        
        $this->Auth->allow(['users']);
    }

    /**
     * Login method to users
     *
     * @return \Cake\Network\Response|null Redirects on successful login, renders view otherwise.
     */
    public function users()
    {
        if ($this->Auth->user()) {
            return $this->redirect($this->Auth->redirectUrl());
        }

        if ($this->request->is('post')) {
            $user = $this->Auth->identify();

            if ($user && $user['status'] === 'active') {
                $this->setSessionData($user);
                return $this->redirect($this->Auth->redirectUrl());
            }

            $this->Flash->error(__('Invalid email or password.'));
        }
    }

    /**
     * Save user permissions in session.
     *
     * @param array $user User data.
     */
    private function setSessionData($user)
    {
        $this->loadModel('Users');

        $profile = $this->Users->Profiles->get($user['profile_id'], [
            'contain' => ['Permissions']
        ]);

        $permissions = [];

        foreach ($profile->permissions as $permission) {
            $permission = sprintf('%s.%s', $permission->controller, $permission->action);
            array_push($permissions, strtolower($permission));
        }

        $user['permissions'] = $permissions;
        $this->Auth->setUser($user);
    }
}
