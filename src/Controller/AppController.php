<?php
/**
 * CakePHP(tm) : Rapid Development Framework (http://cakephp.org)
 * Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * @copyright Copyright (c) Cake Software Foundation, Inc. (http://cakefoundation.org)
 * @link      http://cakephp.org CakePHP(tm) Project
 * @since     0.2.9
 * @license   http://www.opensource.org/licenses/mit-license.php MIT License
 */
namespace App\Controller;

use Cake\Controller\Controller;
use Cake\Event\Event;
use Cake\Core\Configure;

/**
 * Application Controller
 *
 * Add your application-wide methods in the class below, your controllers
 * will inherit them.
 *
 * @link http://book.cakephp.org/3.0/en/controllers.html#the-app-controller
 */
class AppController extends Controller
{

    /**
     * Initialization hook method.
     *
     * Use this method to add common initialization code like loading components.
     *
     * e.g. `$this->loadComponent('Security');`
     *
     * @return void
     */
    public function initialize()
    {
        parent::initialize();
        
        Configure::load('secure_paths', 'default');

        $this->loadComponent('RequestHandler');
        $this->loadComponent('Flash');
        $this->loadComponent('Csrf');
        
        $this->loadComponent('Auth', [
            'authorize' => ['Controller'],
            'authenticate' => [
                'Form' => [
                    'fields' => ['username' => 'email', 'password' => 'password']
                ]
            ],
            'loginAction' => [
                'controller' => 'Login',
                'action' => 'users'
            ],
            'loginRedirect' => [
                'controller' => 'Users',
                'action' => 'index'
            ]
        ]);
    }

    /**
     * Before render callback.
     *
     * @param \Cake\Event\Event $event The beforeRender event.
     * @return \Cake\Network\Response|null|void
     */
    public function beforeRender(Event $event)
    {
        if (!array_key_exists('_serialize', $this->viewVars) &&
            in_array($this->response->type(), ['application/json', 'application/xml'])
        ) {
            $this->set('_serialize', true);
        }
    }

    /**
     * Authorize access.
     *
     * @param array $user User to authorize.
     * @return boolean True when has access. False otherwise.
     */
    public function isAuthorized($user)
    {
        $securePaths   = $this->getProtectedPaths();
        $requestedPath = $this->getRequestedPath();
        $protected     = in_array($requestedPath, $securePaths);
        $isRoot        = isset($user['is_root']) && $user['is_root'];
        $permissions   = isset($user['permissions']) ? $user['permissions'] : [];

        if (!$protected || $isRoot || in_array($requestedPath, $permissions)) {
            return true;
        }

        return false;
    }

    /**
     * Get protected paths.
     *
     * @return array Paths.
     */
    public function getProtectedPaths()
    {
        return \Cake\Core\Configure::read('protected_paths');
    }

    /**
     * Get current url requested
     *
     * @return string Url
     */
    public function getRequestedPath()
    {
        return strtolower($this->name . '.' . $this->request->params['action']);
    }
}
