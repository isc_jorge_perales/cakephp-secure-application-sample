<?php
use Migrations\AbstractSeed;

/**
 * ProfilesPermissions seed.
 */
class ProfilesPermissionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'profile_id' => '1',
                'permission_id' => '1',
            ],
            [
                'id' => '2',
                'profile_id' => '1',
                'permission_id' => '2',
            ],
            [
                'id' => '3',
                'profile_id' => '1',
                'permission_id' => '3',
            ],
            [
                'id' => '4',
                'profile_id' => '1',
                'permission_id' => '4',
            ],
            [
                'id' => '5',
                'profile_id' => '1',
                'permission_id' => '5',
            ],
            [
                'id' => '6',
                'profile_id' => '1',
                'permission_id' => '6',
            ],
            [
                'id' => '7',
                'profile_id' => '1',
                'permission_id' => '7',
            ],
            [
                'id' => '8',
                'profile_id' => '1',
                'permission_id' => '8',
            ],
            [
                'id' => '9',
                'profile_id' => '1',
                'permission_id' => '9',
            ],
        ];

        $table = $this->table('profiles_permissions');
        $table->insert($data)->save();
    }
}
