<?php
use Migrations\AbstractSeed;

/**
 * Profiles seed.
 */
class ProfilesSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Administrator',
                'profile_key' => 'ROLE_ADMINISTRATOR',
            ],
        ];

        $table = $this->table('profiles');
        $table->insert($data)->save();
    }
}
