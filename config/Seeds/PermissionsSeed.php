<?php
use Migrations\AbstractSeed;

/**
 * Permissions seed.
 */
class PermissionsSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'name' => 'Consultar',
                'controller' => 'users',
                'action' => 'index',
                'module_id' => '1',
            ],
            [
                'id' => '2',
                'name' => 'Agregar',
                'controller' => 'users',
                'action' => 'add',
                'module_id' => '1',
            ],
            [
                'id' => '3',
                'name' => 'Editar',
                'controller' => 'users',
                'action' => 'edit',
                'module_id' => '1',
            ],
            [
                'id' => '4',
                'name' => 'Eliminar',
                'controller' => 'users',
                'action' => 'delete',
                'module_id' => '1',
            ],
            [
                'id' => '5',
                'name' => 'Consultar',
                'controller' => 'profiles',
                'action' => 'index',
                'module_id' => '2',
            ],
            [
                'id' => '6',
                'name' => 'Agregar',
                'controller' => 'profiles',
                'action' => 'add',
                'module_id' => '2',
            ],
            [
                'id' => '7',
                'name' => 'Editar',
                'controller' => 'profiles',
                'action' => 'edit',
                'module_id' => '2',
            ],
            [
                'id' => '8',
                'name' => 'Permisos',
                'controller' => 'profiles',
                'action' => 'permissions',
                'module_id' => '2',
            ],
            [
                'id' => '9',
                'name' => 'Eliminar',
                'controller' => 'profiles',
                'action' => 'delete',
                'module_id' => '2',
            ],
        ];

        $table = $this->table('permissions');
        $table->insert($data)->save();
    }
}
