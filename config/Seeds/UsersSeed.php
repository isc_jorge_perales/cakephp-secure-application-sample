<?php
use Migrations\AbstractSeed;

/**
 * Users seed.
 */
class UsersSeed extends AbstractSeed
{
    /**
     * Run Method.
     *
     * Write your database seeder using this method.
     *
     * More information on writing seeds is available here:
     * http://docs.phinx.org/en/latest/seeding.html
     *
     * @return void
     */
    public function run()
    {
        $data = [
            [
                'id' => '1',
                'first_name' => 'Jorge',
                'last_name' => 'Perales',
                'second_last_name' => '',
                'email' => 'jperales@syesoftware.com',
                'password' => '$2y$10$EpFVumKyMpp.XUqNv1iUKuOV0gnwayK/oyrGLVssk7Sy8K89.e0PG',
                'status' => 'active',
                'date_added' => '2017-09-14 01:41:00',
                'profile_id' => '1',
                'is_root' => '1',
            ],
        ];

        $table = $this->table('users');
        $table->insert($data)->save();
    }
}
