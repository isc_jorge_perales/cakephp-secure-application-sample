<?php
return [
    'protected_paths' => [
        'users.index',
        'users.add',
        'users.edit',
        'users.delete',
        'profiles.index',
        'profiles.add',
        'profiles.edit',
        'profiles.permissions',
        'profiles.delete',
    ]
];
