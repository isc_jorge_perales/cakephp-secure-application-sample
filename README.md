# CakePHP Sample Secure Application

[![Build Status](https://img.shields.io/travis/cakephp/app/master.svg?style=flat-square)](https://travis-ci.org/cakephp/app)
[![License](https://img.shields.io/packagist/l/cakephp/app.svg?style=flat-square)](https://packagist.org/packages/cakephp/app)

## Installation

1. Download [Composer](https://getcomposer.org/doc/00-intro.md) or update `composer self-update`.
2. Clone the project.
3. Run `php composer.phar install`

If Composer is installed globally, run

```bash
composer install
```

## Configuration

1. Read and edit `config/app.php` and setup the `'Datasources'` and any other configuration relevant for your application. Use the security salt `afc402ade323315f55623d93eb8b23402585a4f55c7a3660a58efeb928d5e354` for demo purposes.
2. Create the database tables.

    ```bash
    bin/cake migrations migrate
    ```

3. Seed database tables with the default data.
   
    ```bash
    bin/cake migrations seed
    ```

## Run

You can now either use your machine's webserver to view the default home page, or start
up the built-in webserver with:

```bash
bin/cake server -p 8765
```

Then visit `http://localhost:8765` to see the welcome page.

## Adding permissions

1 Edit `config/permissions.xml` and add the new permissions. For example:
```xml
<?xml version="1.0" encoding="utf-8"?>
<Security>
    ...
    <Module name="New Module Name">
        <Action name="Permission Name" controller="controller" action="action" />
    </Module>
</Security>
```
2 Import the new permissions.
```bash
bin/cake permissions
```

## Adding authorization checks in templates

You can use the `AuthHelper` for authorization check in templates. This is useful to conditionally display user-interface controls for operations that are protected. For example:

```php
<?php if ($this->Auth->check('users.delete')) : ?>
    <a href="users/delete/1">Delete</a>
<?php endif; ?>
```