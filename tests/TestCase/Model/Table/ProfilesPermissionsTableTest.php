<?php
namespace App\Test\TestCase\Model\Table;

use App\Model\Table\ProfilesPermissionsTable;
use Cake\ORM\TableRegistry;
use Cake\TestSuite\TestCase;

/**
 * App\Model\Table\ProfilesPermissionsTable Test Case
 */
class ProfilesPermissionsTableTest extends TestCase
{

    /**
     * Test subject
     *
     * @var \App\Model\Table\ProfilesPermissionsTable
     */
    public $ProfilesPermissions;

    /**
     * Fixtures
     *
     * @var array
     */
    public $fixtures = [
        'app.profiles_permissions',
        'app.profiles',
        'app.users',
        'app.permissions',
        'app.modules'
    ];

    /**
     * setUp method
     *
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $config = TableRegistry::exists('ProfilesPermissions') ? [] : ['className' => ProfilesPermissionsTable::class];
        $this->ProfilesPermissions = TableRegistry::get('ProfilesPermissions', $config);
    }

    /**
     * tearDown method
     *
     * @return void
     */
    public function tearDown()
    {
        unset($this->ProfilesPermissions);

        parent::tearDown();
    }

    /**
     * Test initialize method
     *
     * @return void
     */
    public function testInitialize()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test validationDefault method
     *
     * @return void
     */
    public function testValidationDefault()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }

    /**
     * Test buildRules method
     *
     * @return void
     */
    public function testBuildRules()
    {
        $this->markTestIncomplete('Not implemented yet.');
    }
}
